package com.bellalhrlux.pizzashop.ui.register;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bellalhrlux.pizzashop.R;
import com.bellalhrlux.pizzashop.models.Users;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFrag extends Fragment {

    View rootView;
    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etEamil)
    TextInputEditText etEamil;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.btnRegister)
    Button btnRegister;

    RegisterViewModel registerViewModel;

    public RegisterFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, rootView);

        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        return rootView;
    }

    private void processData(Users users) {
        registerViewModel.insertUserToDatabase(users).observe(this, new Observer<Long>() {
            @Override
            public void onChanged(Long aLong) {
                if(aLong>0)
                {
                    Toast.makeText(getContext(), "Successfully Insert", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getContext(), "Insertion Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @OnClick(R.id.btnRegister)
    public void registerBtnClicked()
    {
        String name=etName.getText().toString();
        String email=etEamil.getText().toString();
        String pass=etPassword.getText().toString();

        if(TextUtils.isEmpty(name))
        {
            etName.setError("Enter your name");
            return;
        }
        if(TextUtils.isEmpty(email))
        {
            etEamil.setError("Enter your email");
            return;
        }
        if(TextUtils.isEmpty(pass))
        {
            etPassword.setError("Enter your password");
            return;
        }

        processData(new Users(name,email,pass));


    }

}
