package com.bellalhrlux.pizzashop.ui.intro;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.bellalhrlux.pizzashop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroFrag extends Fragment {

    View rootView;
    @BindView(R.id.loginBtn)
    Button loginBtn;
    @BindView(R.id.registerBtn)
    Button registerBtn;

    public IntroFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_intro, container, false);
        ButterKnife.bind(this,rootView);
        return rootView;
    }

    @OnClick(R.id.loginBtn)
    public void loginBtnClicked()
    {
        Navigation.findNavController(rootView).navigate(R.id.action_introFrag_to_loginFrag);
    }
    @OnClick(R.id.registerBtn)
    public void registerBtnClicked()
    {
        Navigation.findNavController(rootView).navigate(R.id.action_introFrag_to_registerFrag);
    }

}
