package com.bellalhrlux.pizzashop.ui.login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bellalhrlux.pizzashop.database.AppDatabase;
import com.bellalhrlux.pizzashop.models.Users;

public class LoginViewModel extends AndroidViewModel {
    Application application;
    private MutableLiveData<Users> singleUser;
    public LoginViewModel(@NonNull Application application) {
        super(application);
        this.application=application;
        inits();
    }

    private void inits() {
        singleUser=new MutableLiveData<>();
    }

    public LiveData<Users> getLoggedUserInfo(String email,String password)
    {
       Users user=AppDatabase.getInstance(application).userDao().checkedUserAuth(email,password);
       singleUser.setValue(user);

       return singleUser;
    }
}
