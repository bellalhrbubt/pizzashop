package com.bellalhrlux.pizzashop.ui.dashboard;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bellalhrlux.pizzashop.R;
import com.bellalhrlux.pizzashop.models.Users;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    View rootView;

    DashboardViewModel dashboardViewModel;
    @BindView(R.id.userRecyclerView)
    RecyclerView userRecyclerView;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.backBtn)
    ImageView backBtn;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, rootView);
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);
        Bundle bundle = this.getArguments();
        boolean isLogged = bundle.getBoolean("isAdminLogged", false);
        if (isLogged) {
            getAllUserListFromDatabase();
        }
        return rootView;

    }

    private void getAllUserListFromDatabase() {
        dashboardViewModel.getUserList().observe(this, new Observer<List<Users>>() {
            @Override
            public void onChanged(List<Users> users) {
                reloadAdapter(users);
            }
        });
    }

    private void reloadAdapter(List<Users> usersList) {
        LinearLayoutManager glm = new LinearLayoutManager(getContext());
        glm.setOrientation(LinearLayoutManager.VERTICAL);
        userRecyclerView.setLayoutManager(glm);
        userRecyclerView.setAdapter(new UserAdapter(getContext(), usersList));
    }

    @OnClick(R.id.backBtn)
    public void backBtnClicked()
    {
        getActivity().onBackPressed();
    }

}
