package com.bellalhrlux.pizzashop.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.bellalhrlux.pizzashop.R;
import com.bellalhrlux.pizzashop.models.Users;
import com.bellalhrlux.pizzashop.utils.Common;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFrag extends Fragment {

    View rootView;
    @BindView(R.id.etEamil)
    TextInputEditText etEamil;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    LoginViewModel loginViewModel;
    final String TAG="loginPage";

    public LoginFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this,rootView);
        loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);
        return rootView;
    }

    @OnClick(R.id.btnLogin)
    public void btnLoginClicked()
    {
        String email=etEamil.getText().toString();
        String pass=etPassword.getText().toString();

        if(TextUtils.isEmpty(email))
        {
            etEamil.setError("Enter your email");
            return;
        }
        if(TextUtils.isEmpty(pass))
        {
            etPassword.setError("Enter your password");
            return;
        }

        if(email.equals(Common.adminEamil) && pass.equals(Common.adminPassword))
        {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAdminLogged",true);
            Navigation.findNavController(rootView).navigate(R.id.action_loginFrag_to_dashboardFragment,bundle);
        }
        else{
            checkedUserAuthentication(email,pass);
        }


    }

    private void checkedUserAuthentication(String email, String pass) {
        loginViewModel.getLoggedUserInfo(email,pass).observe(this, new Observer<Users>() {
            @Override
            public void onChanged(Users users) {
                if(users!=null)
                {
                   try {
                       Bundle bundle = new Bundle();
                       bundle.putBoolean("isAdminLogged",false);
                       Navigation.findNavController(rootView).navigate(R.id.action_loginFrag_to_dashboardFragment,bundle);
                       //Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                   }
                   catch (Exception e)
                   {
                       Log.e(TAG,e.getMessage());
                   }
                }
                else{
                    Toast.makeText(getContext(), "Invalid email or password !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
