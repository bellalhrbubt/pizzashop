package com.bellalhrlux.pizzashop.ui.register;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import com.bellalhrlux.pizzashop.database.AppDatabase;
import com.bellalhrlux.pizzashop.models.Users;

public class RegisterViewModel extends AndroidViewModel {
    Application application;
    private MutableLiveData<Long> insertSingleUser;
    public RegisterViewModel(@NonNull Application application) {
        super(application);
        this.application=application;
        inits();
    }

    private void inits() {
        insertSingleUser=new MutableLiveData<>();
    }

    public LiveData<Long> insertUserToDatabase(Users users)
    {
       long row=AppDatabase.getInstance(application).userDao().insertSingleUser(users);
       insertSingleUser.setValue(row);

       return insertSingleUser;

    }
}
