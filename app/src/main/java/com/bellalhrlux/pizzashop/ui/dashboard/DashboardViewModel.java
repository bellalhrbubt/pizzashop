package com.bellalhrlux.pizzashop.ui.dashboard;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bellalhrlux.pizzashop.database.AppDatabase;
import com.bellalhrlux.pizzashop.models.Users;

import java.util.List;

public class DashboardViewModel extends AndroidViewModel {
    Application application;
    private MutableLiveData<List<Users>> userList;

    public DashboardViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        inits();
    }

    private void inits() {
        userList = new MutableLiveData<>();
    }

    public LiveData<List<Users>> getUserList() {
        List<Users> user = AppDatabase.getInstance(application).userDao().getAllUserList();
        userList.setValue(user);

        return userList;
    }
}
