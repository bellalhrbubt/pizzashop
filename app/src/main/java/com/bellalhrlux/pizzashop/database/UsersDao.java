package com.bellalhrlux.pizzashop.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.bellalhrlux.pizzashop.models.Users;

import java.util.List;

@Dao
public interface UsersDao {
    @Query("SELECT * FROM users")
    List<Users> getAllUserList();

    @Insert
    void insertAll(Users... users);

    @Insert
    long insertSingleUser(Users users);

    @Query("select *from users where  email= :emailVal and password= :passwordVal")
    Users checkedUserAuth(String emailVal, String passwordVal);
}
